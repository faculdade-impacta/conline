import http from './httpServices';

export const getCurrentUser = () => {
    return localStorage.getItem('caas_id_token') || '';
}

export const checkApi = () => {
    http.get('/').then(
        res => console.log(res.data.data)
        ).catch(err => console.log('Erro na Api: ', err));
}