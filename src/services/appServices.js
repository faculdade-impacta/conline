import AppActions from '../app/root/app.actions';

export const registerSearch = (searchParameter) => (dispatch) => {{
    dispatch({
        type: AppActions.MAKE_SEARCH,
        payload: searchParameter
    });
}};