const size = {
  mobileS: '320px'
  , hmobileWide: '360px'
  , mobileM: '375px'
  , mobileL: '425px'
  , miniTablet: '599px'
  , tablet: '768px'
  , lMobileWide: '640'
  , mobileWide: '812'
  , laptopS: '992px'
  , laptopM: '1024px'
  , desktop: '1530px'
};

export const device = {
  mobile: `(max-width: ${size.mobileL})`
  , miniTablet: `(max-width: ${size.miniTablet} ) and (min-width: ${size.mobileL})`
  , tablet: `(max-width: ${size.tablet} ) and (min-width: ${size.mobileL})`
  , laptop: `(max-width: ${size.laptopS}) and (min-width: ${size.tablet})`
  , desktop: `(max-width: ${size.desktop}) and (min-width: ${size.laptopS})`
  , lMobileWide: `(min-width: ${size.lMobileWide})`
  , mobileWide: `(max-width: ${size.mobileWide}) and (min-width: ${size.mobileL})`
};
