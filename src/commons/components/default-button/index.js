import React from "react";
import styled from "styled-components";
import { Button } from '@material-ui/core'

const DefaultButton = styled(Button)`
    float: right;
    width: 223px;
    height: 42px;
    background-color: #00BCD4 !important;
    color: #FFF !important;
    position: inherit !important;
`;

export default DefaultButton;