import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

const defaultLink = styled(Link)`
    color: #5C5C5C;
    :hover {
        color: #FFF;
        text-decoration: none;
    }
`;

export default defaultLink;