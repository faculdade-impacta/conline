import styled from "styled-components";
import { TextField, InputAdornment, IconButton } from '@material-ui/core';
import { Visibility, VisibilityOff } from '@material-ui/icons';

export const InputElements = {
    TextField: styled(TextField)`

    `,
    InputAdornment: styled(InputAdornment)`
        
    `,
    IconButton: styled(IconButton)`
        
    `,
    IconButton: styled(IconButton)`
        
    `,
    Visibility: styled(Visibility)`
        
    `,
    VisibilityOff: styled(VisibilityOff)`
        
    `,
};