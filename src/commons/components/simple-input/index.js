import React, { Component } from "react";
import { InputElements } from "./style";

class InputSimple extends Component {
    render() {
        return (
            <InputElements.TextField
                defaultValue={this.props.defaultValue}
                ref={this.props.ref}
                value={this.props.value}
                name={this.props.name}
                type={this.props.type}
                label={this.props.label}
                onChange={this.props.onChange}
                margin={this.props.margin || "normal"}
                variant={this.props.variant || "filled"}
                InputLabelProps={this.props.InputLabelProps}
                rows={this.props.rows}
                multiline={true}
                disabled={this.props.disabled}
                fullWidth
            />
        );
    }
} export default InputSimple;