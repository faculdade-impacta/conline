import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from 'react-router-dom';
import { loadSearch } from './professional-form.actions';
import MenuNavbar from "../../commons/containers/navbar";
import { Wrappers } from "./professional-form.style";
import { cleanErrors } from '../root/app.actions';
import { StructureLayout } from "../../commons/layout";
import Footer from "../../commons/tovo-components/footer";

class ProfessionalFormPage extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        this.props.cleanErrors();
        const { query, region } = this.props.match.params;
        this.props.loadSearch(region, query);
    }

    render() {
        return (
            <div className="app-professional-form">
                <MenuNavbar />
                <Wrappers.header>
                </Wrappers.header>
                <StructureLayout.Container className="form-container">
                    <StructureLayout.Row >
                        <StructureLayout.Col md="6">
                            <StructureLayout.Row>
                                <StructureLayout.Col md="12">
                                    <h3>Seja um cuidador.<br></br><br></br>
                                        <small>
                                            <p>A plataforma Cuidador Online tem como objetivo a democratização do acesso ao cuidado.</p>
                                            <p>Se você tem experiência, contato com a área e ostaria de se juntar a plataforma para receber ofertas de trabalho ? </p>
                                        </small>
                                    </h3>
                                </StructureLayout.Col>
                                <StructureLayout.Col md="12">
                                    <StructureLayout.Row>
                                        <StructureLayout.Col className="imgContainer" md="6" sm="6" xs="6">
                                            <img className="img-cuidadora" src="assets/images/app/enfermeira-cadastro.svg" />
                                        </StructureLayout.Col>
                                        <StructureLayout.Col md="6" sm="6" xs="6">
                                            <p>Simples! Preencha o formulário e entraremos em contato :)</p>
                                            <p>
                                                Estamos montando o time de cuidadores online, seja um dos primeiros a se juntar
                                                a comunidade dos cuidadores do cuidador.online.
                                            </p>
                                        </StructureLayout.Col>
                                    </StructureLayout.Row>
                                </StructureLayout.Col>
                            </StructureLayout.Row>
                        </StructureLayout.Col>
                        <StructureLayout.Col md="6">
                            <form action="https://docs.google.com/forms/d/e/1FAIpQLSdKuFwBFZe1fhBc2aI3Hz0ns5VOCk37ozSGNF3jxa2l0YeSAg/formResponse"
                                target="_self"
                                id="professional-form"
                                method="POST">

                                <fieldset>
                                    <span for="">Email</span>
                                    <div class="form-group">
                                        <input id="emailAddress" type="email" name="emailAddress" class="form-control" required />
                                    </div>
                                </fieldset>


                                {/* <!-- Field type: "short" id: "1050223662" --> */}
                                <fieldset>
                                    <span for="1050223662">Nome completo</span>
                                    <div class="form-group">
                                        <input id="1650096272" type="text" name="entry.1650096272" class="form-control" required />
                                    </div>
                                </fieldset>


                                {/* <!-- Field type: "short" id: "1120746642" --> */}
                                <fieldset>
                                    <span for="1120746642">Número de celular</span>
                                    <div class="form-group">
                                        <input id="1743374944" type="text" name="entry.1743374944" class="form-control" required />
                                    </div>
                                </fieldset>


                                {/* <!-- Field type: "short" id: "1454919671" --> */}
                                <fieldset>
                                    <span for="1454919671">CPF</span>
                                    <div class="form-group">
                                        <input id="1585466031" type="text" name="entry.1585466031" class="form-control" required />
                                    </div>
                                </fieldset>


                                {/* <!-- Field type: "short" id: "2033243489" --> */}
                                <fieldset>
                                    <span for="2033243489">CEP</span>
                                    <div class="form-group">
                                        <input id="1612563382" type="text" name="entry.1612563382" class="form-control" required />
                                    </div>
                                </fieldset>


                                {/* <!-- Field type: "date" id: "252098968" --> */}
                                <fieldset>
                                    <span for="252098968">Data de nascimento</span>
                                    <div class="form-group">
                                        <input type="date" id="37799665_date" placeholder="20/02/2019" class="form-control" required />
                                    </div>
                                </fieldset>


                                {/* <!-- Field type: "choices" id: "2066600204" --> */}
                                <fieldset>
                                    <span for="2066600204">Sexo</span>
                                    <div class="form-group">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="entry.1217998698" value="Masculino" required />
                                                Masculino
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="entry.1217998698" value="Feminino" required />
                                                Feminino
                                            </label>
                                        </div>
                                    </div>
                                </fieldset>


                                {/* <!-- Field type: "checkboxes" id: "775888569" --> */}
                                <fieldset>
                                    <span for="775888569">Formação</span>
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="entry.1886862369" value="Possui curso técnico?" />
                                                Possui curso técnico?
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="entry.1886862369" value="Possui graduação?" />
                                                Possui graduação?
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="entry.1886862369" value="Certificações na área?" />
                                                Certificações na área?
                                            </label>
                                        </div>
                                    </div>
                                </fieldset>


                                {/* <!-- Field type: "checkboxes" id: "1708599103" --> */}
                                <fieldset>
                                    <span for="1708599103">Quais tipos de serviços tem experiência?</span>
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="entry.302828427" value="Companhia" />
                                                Companhia
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="entry.302828427" value="Acompanhamento para consulta" />
                                                Acompanhamento para consulta
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="entry.302828427" value="Acompanhamento em atividades rotineiras (ex: mercado)" />
                                                Acompanhamento em atividades rotineiras (ex: mercado)
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="entry.302828427" value="Administração de medicação" />
                                                Administração de medicação
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="entry.302828427" value="Atividades domésticas" />
                                                Atividades domésticas
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="entry.302828427" value="Banho" />
                                                Banho
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="entry.302828427" value="Refeição" />
                                                Refeição
                                            </label>
                                        </div>
                                    </div>
                                </fieldset>


                                {/* <!-- Field type: "choices" id: "1202846374" --> */}
                                <fieldset>
                                    <span for="1202846374">Tem familiaridade com aplicativos de transporte. Ex: Uber, 99, Cabify ?</span>
                                    <div class="form-group">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="entry.543129733" value="Sim" required />
                                                Sim
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="entry.543129733" value="Não" required />
                                                Não
                                            </label>
                                        </div>
                                    </div>
                                </fieldset>


                                {/* <!-- Field type: "paragraph" id: "1381671460" --> */}
                                <fieldset>
                                    <span for="1381671460">Fale um pouco de sua experiência como cuidador.</span>
                                    <div class="form-group">
                                        <textarea id="1136279785" name="entry.1136279785" class="form-control" required></textarea>
                                    </div>
                                </fieldset>

                                <input type="hidden" name="fvv" value="1" />
                                <input type="hidden" name="fbzx" value="-2624168161455192290" />
                                {/* <!--
                                    CAVEAT: In multipages (multisection) forms, *pageHistory* field tells to google what sections we've currently completed.
                                    This usually starts as "0" for the first page, then "0,1" in the second page... up to "0,1,2..N" in n-th page.
                                    Keep this in mind if you plan to change this code to recreate any sort of multipage-feature in your exported form.
                                    We're setting this to the total number of pages in this form because we're sending all fields from all the section together.
                                --> */}
                                <input type="hidden" name="pageHistory" value="0" />

                                <input class="btn btn-primary" type="submit" value="Enviar"></input>
                            </form>
                        </StructureLayout.Col>
                    </StructureLayout.Row>
                </StructureLayout.Container>
                <Footer />
            </div>
        )
    }
}

const mapStateToProps = state => ({
    app: state.app,
})
const mapDispatchToProps = dispatch => bindActionCreators({ loadSearch, cleanErrors }, dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ProfessionalFormPage));
