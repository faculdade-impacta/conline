import { ACTIONS } from "./signup.actions"

const register = {
    loading: false,
    error: false,
    registry: null
}

export const signup_reducer = ( cState = register, action ) => {
    switch ( action.type ) {     
        case ACTIONS.CREATING_ACCOUNT:
            return { ...cState, loading: true, error: false, registry: action.payload }
        case ACTIONS.ACOUNT_CREATED:
            return { ...cState, loading: false, error: false, registry: action.payload } 
        case ACTIONS.REGISTER_FAILED:
            return { ...cState, loading: false, error: action.payload, registry: null }
        default:
            return cState
    }
}