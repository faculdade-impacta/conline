import styled from "styled-components";
import { Colors } from "../../../../commons/layout";
import {Button, Label} from "reactstrap";
import TextField from '@material-ui/core/TextField';

export const InputElements = {
    wrap: styled.div`
        width: 100%;
        display: flex;
    `,
    icon: styled.i`
        color: ${ Colors.white };
        font-size: 1.2em;
        padding: 0.2em 0.5em;
        ${ Colors.primaryGradient };
        border-top-left-radius: 2em;
        border-bottom-left-radius: 2em;
    `,
    input: styled.input`
        border: none;
        padding: 0.5em 0.5em;
        width: 100%;
        margin-right: -20px;
    `,
    Button: styled(Button)`
        margin: 0px 10px 0px 10px;
    `,
    Label: styled(Label)`
        margin-right: 10px;
    `
}