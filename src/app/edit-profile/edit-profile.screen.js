import React, { Component } from "react";
import EditProfileForm from "./containers/edit-profile-form";
import MenuNavbar from "../../commons/containers/navbar";

class EditProfile extends Component {
    render() {
        return (
            <div className="app-edit-profile">
                <MenuNavbar />
                <div className="header"></div>
                <EditProfileForm />
            </div>
        )
    }
}

export default EditProfile;
