import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Constants } from "../../../../config/constants"
import noProfilePic from "../../../../../public/assets/images/nopic.png";

class Profile extends Component {
    render() {
        return (
            <div className="col-lg-4 col-md-6">
                <div className={this.props.authenticated ? `review-box` : `review-box-auth`}>
                    <img src={this.props.avatar_id ? `${this.props.avatar_id}` : noProfilePic} alt="review" className="img-fluid" />
                    <div className="row title" title={`${this.props.name} - ${this.props.title}`}>
                        <div className="col-md-12">
                            <h5 className="body-message"><span>{this.props.title}</span></h5>
                            <span className="sub-title">{this.props.categories}</span>
                            <h5 className="body-message">{this.props.name}</h5>
                        </div>
                        <div className="col-md-12 info" title={this.props.authenticated ? `${this.props.location} | ${this.props.phone} | ${this.props.email}` : this.props.location}>
                            <i className="fa fa-map-marker"></i> {this.props.location} {this.props.authenticated ? <a>| <i className="fa fa-phone"></i> {this.props.phone} | <i className="fa fa-envelope"></i> {this.props.email}</a> : ''}
                        </div>
                    </div>
                    <div className="review-message">
                        <p>{this.props.resume}</p>
                    </div>
                    {this.props.authenticated ? '' :
                        <div className="login-to-contact">
                            <Link className="btn btn-primary btn-login" to="/cadastro">Entrar em contato</Link>
                        </div>
                    }
                </div>
            </div>
            // <ProfileElements.container>
            //     <ProfileElements.elementContainer container spacing={24}>
            //         <StructureLayout.Grid item xs={12} sm={2}>
            //             <ProfileElements.Avatar alt="toshiko" src={img} />
            //         </StructureLayout.Grid>
            //         <StructureLayout.Grid item xs={12} sm={10}>
            //             <ProfileElements.title>{this.props.title}</ProfileElements.title>
            //             <ProfileElements.subTitle>{this.props.name} | {this.props.location} | {this.props.categories}</ProfileElements.subTitle>
            //             <ProfileElements.subTitle></ProfileElements.subTitle>
            //             { this.props.authenticated ? <ProfileElements.subTitle><br/>{ this.props.email } | { this.props.phone }</ProfileElements.subTitle> : '' }
            //             <div>{this.props.resume}</div>
            //         </StructureLayout.Grid>
            //     </ProfileElements.elementContainer>
            // </ProfileElements.container>
        )
    }
}

export default Profile