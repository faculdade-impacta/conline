// import Avatar from "../../../../commons/components/profile-avatar";
import { Avatar, Grid } from '@material-ui/core'
import styled from "styled-components";
import  DefaultButton from "../../../../commons/components/default-button";
import { StructureLayout, Fonts } from "../../../../commons/layout";
import { device } from "../../../../commons/devices";

export const ProfileElements = {
    Avatar: styled(Avatar)`
        width: auto !important;
        height: auto !important;
        max-width: 150px;
        max-height: 150px;
        @media ${device.mobile}, ${device.miniTablet} {
            margin: auto !important;
        }
    `,
    container: styled.div`
        margin: 40px 0px 25px 0px;
    `,
    subTitle: styled.span`
        font-weight: 500;
    `,
    title: styled.h3`
    `,
    elementContainer: styled(Grid)`
        margin-bottom: 40px !important;
    `,
    LoginButton: styled(DefaultButton)`
        background-color: #E0F7FA !important;
        border-radius: 25px !important;
        height: 32px;
        width: 160px;
        color: #00BCD4 !important;
        font-size: 12px !important;
        text-align: center;
        white-space: nowrap;
    `
}