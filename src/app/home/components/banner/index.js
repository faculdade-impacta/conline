import React, { Component } from "react";
import { BannerElements } from "./style";
import headerBg from "../../../../static/search-header-low.jpg";
import headerBg2 from "../../../../static/search-header2-low.jpg";

export default () => (
    <BannerElements.Carousel
        autoPlay
        showArrows={false}
        showThumbs={false}
        showStatus={false}
        infiniteLoop={true}
        transitionTime={1000}
        interval={6000}
    >
        {/* <div>
            <BannerElements.BannerImg />
        </div>
        <div>
            <BannerElements.BannerImg />
        </div> */}
        <div>
            <img src={headerBg} />
        </div>
        <div>
            <img src={headerBg2} />
        </div>
    </BannerElements.Carousel >
);