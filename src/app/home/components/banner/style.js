import styled from "styled-components";
import { Carousel } from "react-responsive-carousel";
import { device } from "../../../../commons/devices";

export const BannerElements = {
    Carousel: styled(Carousel)`
        position: absolute;
        .slide {
            height 70vh;
        }
        .control-dots {
            bottom: 165px !important;
        }
        img {
            height: 100vh;
        }
        @media ${device.mobile}, ${device.miniTablet} {
            .control-dots {
                bottom: 64px !important;
            }
            img {
                height 70vh;
            }
            img::after {
                content: '.';
                color: rgba(0,0,0,0);;
                position: absolute;
                width: 100%; height:100%;
                top:0; left:0;
                background:rgba(0,0,0,0.6);
                opacity: 0.6;
                transition: all 1s;
                -webkit-transition: all 1s;
            }
        }
    `,
    BannerImg: styled.img`
        width: 100%;
        position: absolute;
        background-image: url(${headerBg});
        background-repeat: no-repeat;
        background-size: 100% 100vh;
        height: 70vh;
        ::after {
            content: '.';
            color: rgba(0,0,0,0);;
            position: absolute;
            width: 100%; height:100%;
            top:0; left:0;
            background:rgba(0,0,0,0.6);
            opacity: 0.6;
            transition: all 1s;
            -webkit-transition: all 1s;
        }
    `,
}