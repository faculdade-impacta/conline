import React, { Component } from "react";
import { InformativeElements } from "./style";
import { LocalHospital } from "@material-ui/icons"
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    root: {
        color: theme.palette.text.primary,
    },
    icon: {
        margin: theme.spacing.unit,
        fontSize: 32,
        alignSelf: "flex-start",
        position: "absolute",
    },
});

class Informative extends Component {
    render() {
        const { classes } = this.props;
        return (
            <InformativeElements.container elevation={1}>
                <LocalHospital className={classes.icon} />
                <InformativeElements.message>{this.props.message}</InformativeElements.message>
            </InformativeElements.container>
        )
    }
}

export default withStyles(styles)(Informative);