import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import MenuNavbar from "../../commons/containers/navbar";
import { StructureLayout } from "../../commons/layout";
import Search from "./container/search/";
import Footer from "../../commons/tovo-components/footer";
import Feature from "../../commons/components/feature";
import { Link, withRouter } from "react-router-dom";
import Typing from 'react-typing-animation';

class HomePage extends Component {
    render() {
        const { authenticated, user } = this.props.session;
        return (
            <div>
                <div className="app-home">
                    <MenuNavbar />
                    <StructureLayout.Row>
                        <div className="wrapper-banner">
                            <div id="backgroundBlue"></div>
                        </div>
                        <StructureLayout.Container>
                            <StructureLayout.Row className="wrapper-content">
                                <StructureLayout.Col md="12" lg="6">
                                    <div className="wrap-form">
                                        <img className="simbol" src="assets/images/app/simbolo-negativo.svg" />
                                        {/* <Search/> */}
                                        <div className="wrap-content-form">
                                            {/* <h1>Bem vindo a Conline!</h1> */}
                                            <h1>A forma mais simples de encontrar um cuidador!</h1>
                                            <Typing speed={20} className="typing">
                                                <span>Alguém para acompanhar você em um exame.</span>
                                                <Typing.Reset count={0} delay={500} />
                                                <span>Ou cuidar de um parente quando você precisar.</span>
                                                <Typing.Reset count={1} delay={1000} start={500} />
                                                <span>Profissionais para cuidar sempre que você precisar! 😉</span>
                                            </Typing>
                                            <div className="div-btn-encontrar-cuidador">
                                                {
                                                    authenticated ?
                                                        <Link className="nav-link btn-primary" to="/agendar-cuidador">Solicitar cuidador!</Link>
                                                        :
                                                        <Link className="nav-link btn-primary" to="/cadastro">Solicitar cuidador!</Link>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                </StructureLayout.Col>
                                {/* TODO REMOVER ESSA DIV */}
                                <StructureLayout.Col md="1" lg="1">
                                </StructureLayout.Col>
                                <StructureLayout.Col className="desktop" md="5" lg="5">
                                    <div className="home-pic-01"></div>
                                </StructureLayout.Col>
                            </StructureLayout.Row>
                            <div className="wrap-arrow desktop">
                                <a href="#como-funciona"><img className="arrow-scroll rubberBand" src="assets/images/app/arrow.svg" /></a>
                            </div>
                        </StructureLayout.Container>
                    </StructureLayout.Row>

                    <StructureLayout.Row id="como-funciona" className="bg-como-funciona">
                        <StructureLayout.Container>
                            <Feature></Feature>
                            <div className="wrap-arrow desktop">
                                <a href="#sobre"><img className="arrow-scroll rubberBand" src="assets/images/app/arrow.svg" /></a>
                            </div>
                        </StructureLayout.Container>
                    </StructureLayout.Row>


                    <StructureLayout.Row id="sobre" className="bg-white">
                        <StructureLayout.Container>
                            <StructureLayout.Row className="wrap-about">
                                <StructureLayout.Col className="desktop" md="12" lg="3">
                                    <div className="home-pic-02"></div>
                                </StructureLayout.Col>
                                <StructureLayout.Col className="desktop" md="1" lg="1">
                                </StructureLayout.Col>
                                <StructureLayout.Col md="12" lg="8">
                                    {/* <div className="home-pic-03"></div> */}

                                    <div className="">
                                        <h1>Conheça a <strong>Cuidador</strong> Online</h1>
                                        <p>
                                            A C.Online é uma plataforma para a comunidade de profissionais de assistência anunciarem seus serviços. Nosso objetivo é oferecer uma plataforma inclusiva, que diminua a distância entre os profissionais de assistência e pessoas que precisam de cuidados.
                                        </p>
                                        <img className="simbol-02" src="assets/images/app/marca-positivo.svg" />
                                    </div>
                                </StructureLayout.Col>
                            </StructureLayout.Row>
                        </StructureLayout.Container>
                    </StructureLayout.Row>
                </div>
                <Footer />
            </div >
        )
    }
}

const mapStateToProps = state => ({
    session: state.session
});

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);

