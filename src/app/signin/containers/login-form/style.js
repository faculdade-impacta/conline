import styled from "styled-components";
import { Paper, Button } from "@material-ui/core";

import { Fonts, Colors } from "../../../../commons/layout";
import InputPassword from "../../../../commons/components/password-input";
import InputSimple from "../../../../commons/components/simple-input";
import defaultLink from "../../../../commons/components/link";

export const LoginFormsElements = {
    wrapLoader: styled.div`
        display: flex;
        justify-content: center;
        flex-direction: column;
        margin: auto;
        height: 100%;
        width: 50px;
    `,
    wrapButtons: styled.div`
        display: flex;
        flex-direction: column;
        text-align: center;
        margin: 15px auto 0 auto;
        width: 200px;

    `,
    Button: styled(Button)`
        background-color: ${ Colors.primaryColor}!important;
        color: white!important;
        margin: 0 0 15px 0 !important;
    `,
    Username: styled(InputSimple)`
    
    `,
    Password: styled(InputPassword)`
    `,
    Link: styled(defaultLink)`
        color: #007bff;
        &:hover, &:focus {
            color: #007bff;
            text-decoration: none;
            outline: none;
        }
    `,
    Paper: styled(Paper)`
        max-width: 700px;
        margin: auto;
        padding: 30px 60px; 
        min-width: 300px;
    `,
    resetPassword: styled(Fonts.simpleLink)`
        display: flex;
        justify-content: flex-end;
        font-size: 12px;
    `,
    error: styled.h2`
        text-align: center;
        color: #F44336;
        font-family: 'Raleway',sans-serif;
        font-size: 19px;
    `,
}