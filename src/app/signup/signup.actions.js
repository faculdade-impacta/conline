import { userSignUp, editUserInfo, changePassword } from "../../services/userServices";
import AppActions from '../root/app.actions'

export const createNewUser = (user) => (dispatch) => {
    dispatch({
        type: AppActions.APP_LOADING,
        payload: null
    });
    userSignUp(user).then(
        res => {
            dispatch({
                type: AppActions.MAKE_LOGIN,
                payload: res
            });
        }
    ).catch(err => {
        dispatch({
            type: AppActions.APP_ERROR,
            payload: err
        });
    });
}

export const editUser = (attributes) => (dispatch) => {
    dispatch({
        type: AppActions.APP_LOADING,
        payload: null
    });
    editUserInfo(attributes).then(
        res => {
            dispatch({
                type: AppActions.MAKE_LOGIN,
                payload: res
            });
        }
    ).catch(err => {
        dispatch({
            type: AppActions.APP_ERROR,
            payload: err
        });
    });
}

export const changePass = (attributes) => (dispatch) => {
    dispatch({
        type: AppActions.APP_LOADING,
        payload: null
    });
    changePassword(attributes).then(
        res => {
            dispatch({
                type: AppActions.MAKE_LOGIN,
                payload: res
            });
        }
    ).catch(err => {
        console.log(err);
        dispatch({
            type: AppActions.APP_ERROR,
            payload: err
        });
    });
}