import styled from "styled-components";
import { Button, FormLabel, Radio, Paper } from '@material-ui/core';
import { device } from "../../../../commons/devices"

export const RegisterElements = {
    radio: styled(Radio)`
    `,
    formLabel: styled(FormLabel)`
    
    `,
    wrap: styled.div`
        display: flex;
        flex-direction: row;
        max-width: 600px;
        align-items: center;
    `,
    form: styled.form`
        margin: 0 auto;
        width: 50%;
        padding: 25px;
        background: #FFF;
        @media ${device.mobile}, ${device.miniTablet}, ${device.tablet} {
            width: 100%;
        }
    `,
    Button: styled(Button)`
        float: right;
        margin-top: 40px !important;
        margin-bottom: 40px !important;
        width: 223px;
        background-color: #00BCD4 !important;
    `,
    Paper: styled(Paper)`
    max-width: 700px;
    margin: auto;
    padding: 30px 60px;
    height: 400px;
    min-width: 300px;
    `,
    wrapLoader: styled.div`
        display: flex;
        justify-content: center;
        flex-direction: column;
        margin: auto;
        height: calc(100vh - 56px);
        width: 50px;
    `,
    wrapMessage: styled.div`
        display: flex;
        justify-content: center;
        flex-direction: column;
        height: calc(100vh - 56px);
        width: 100%;
        max-width: 1500px;
        margin: auto;
    `,
    message: styled.h1`
        text-align: center;
        font-family: 'Raleway', sans-serif;
    `,
    error: styled.div`
        width: 100%;
        background-color: #f443364f;
        border: 1px solid #F44336;
        color: #F44336;
        margin-top: -5px;
        padding: 15px 20px;
        text-align: center;
        font-family: 'Raleway', sans-serif;
        font-weight: 600;
        margin-bottom: 20px;
    `
}