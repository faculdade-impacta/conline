import AppActions from "../root/app.actions";
import http from "../../services/httpServices";

export const getListServices = () => (dispatch) => {
    dispatch({
        type: AppActions.APP_LOADING,
        payload: null
    });
    http
        .get(`/service`)
        .then(response => {
            dispatch({
                type: AppActions.UPDATE_PUBLISHED_SERVICESS,
                payload: response.data
            });
        }).catch((error) => {
            dispatch({
                type: AppActions.APP_ERROR,
                payload: error
            });
        });
}

export const deleteService = (serviceId, services) => (dispatch) => {
    http
        .delete(`/service/${serviceId}`)
        .then((response) => {
            dispatch({
                type: AppActions.UPDATE_PUBLISHED_SERVICESS,
                payload: services.filter(element => element.id != serviceId)
            });
        }).catch((error) => {
            dispatch({
                type: AppActions.APP_ERROR,
                payload: error
            });
        });
}

export const createService = (service) => (dispatch) => {

    dispatch({
        type: AppActions.APP_LOADING,
        payload: null
    });

    http
        .post(`/service/`, service)
        .then(response => {
            dispatch({
                type: AppActions.UPDATE_PUBLISHED_SERVICESS,
                payload: response.data
            });
        }).catch((error) => {
            dispatch({
                type: AppActions.APP_ERROR,
                payload: error
            });
        });
}

export const updateService = (service, services) => (dispatch) => {
    dispatch({
        type: AppActions.UPDATE_PUBLISHED_SERVICESS,
        payload: services.filter(element => element.id !== service.id).concat(service)
    });
    http
        .put(`/service/${service.id}`, service)
        .then().catch((error) => {
            dispatch({
                type: AppActions.APP_ERROR,
                payload: error
            });
        });
}