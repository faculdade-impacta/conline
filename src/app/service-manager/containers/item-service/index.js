import React, { Component } from "react";
import { ServiceElements, CreateUpdateElements } from "./style";
import { StructureLayout } from "../../../../commons/layout";
import { Modal, ModalHeader, ModalBody, Col } from 'reactstrap';
import { deleteService, updateService } from "../../service-manager.actions";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import InputMask from 'react-input-mask';
import { ModalElements } from '../../../../commons/components/modal/style';
import ConfirmModal from '../../../../commons/components/confirm-modal'


class ServiceItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            service: this.props.service
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.toggle = this.toggle.bind(this);
        this.deleteService = this.deleteService.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    toggle() {
        this.setState({
            modal: !this.state.modal,
            service: this.props.service
        });
    }

    onSubmit(event) {
        event.preventDefault();

        if (event.target.service_phone.value.replace(/[^0-9.]/g, "").length < 11) {
            alert("Por favor preencher número de telefone completo")
            return
        }
        if (event.target.resume.value.length < 180) {
            alert("Por favor no mínimo 180 caracteres")
            return
        }
        let service = {
            id: this.state.service.id,
            title: event.target.title.value,
            categories: event.target.categories.value,
            resume: event.target.resume.value,
            service_phone: event.target.service_phone.value,
            service_email: event.target.service_email.value
        }
        //TODO VALIDAR SE OS CAMPOS NAO ESTAO EM BRANCO
        this.props.updateService(service, this.props.publishedServices);
        this.setState({
            modal: !this.state.modal
        });
        return null
    }

    handleInputChange(e) {
        this.setState({
            service: {
                ...this.state.service, [e.target.name]: e.target.value.toString()
            }
        })
    }

    deleteService(serviceId, services) {
        this.props.deleteService(serviceId, services);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            service: nextProps.service
        });
    }

    render() {
        return (
            <StructureLayout.Grid>
                <div className="service-parent">
                    <ServiceElements.serviceText>
                        <ServiceElements.title>{this.state.service.title}</ServiceElements.title>
                        <ServiceElements.subTitle>{this.state.service.categories}</ServiceElements.subTitle>
                        <p className="descTag">{this.state.service.resume}</p>
                    </ServiceElements.serviceText>
                    <ServiceElements.options className="option-buttons">
                        <ServiceElements.edit>
                            <button className="btn-primary" value="Editar" onClick={this.toggle}>Editar</button>
                        </ServiceElements.edit>
                        <ServiceElements.delete>
                            <button className="btn-primary" data-toggle="modal" data-target={`#serviceId-${this.state.service.id}`}>Deletar</button>
                            <ConfirmModal
                                serviceId={`serviceId-${this.state.service.id}`}
                                onClick={() => this.deleteService(this.state.service.id, this.props.publishedServices)}
                            />
                        </ServiceElements.delete>
                    </ServiceElements.options>
                </div>
                <hr />


                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalElements.contentStyle>
                        <ModalHeader toggle={this.toggle}>Anúnciar Serviço</ModalHeader>
                        <ModalBody>
                            <form className="formServiceModal" onSubmit={this.onSubmit}>
                                <Col md="8" xs="12">
                                    <CreateUpdateElements.flexDiv>
                                        <CreateUpdateElements.titleContainer>
                                            <input
                                                required={true}
                                                placeholder="Título"
                                                maxLength={20}
                                                name="title"
                                                label="Titulo"
                                                id="title"
                                                value={this.state.service.title}
                                                onChange={this.handleInputChange}
                                            />
                                        </CreateUpdateElements.titleContainer>
                                        <CreateUpdateElements.inputSizer>
                                            <input
                                                required={true}
                                                placeholder="Sub-título"
                                                maxLength={20}
                                                name="categories"
                                                label="Categoria"
                                                id="category"
                                                value={this.state.service.categories}
                                                onChange={this.handleInputChange}
                                            />
                                        </CreateUpdateElements.inputSizer>
                                    </CreateUpdateElements.flexDiv>
                                    <textarea
                                        required={true}
                                        placeholder="Descreva um breve resumo sobre o serviço :)."
                                        name="resume"
                                        label="Descricao"
                                        rows="12"
                                        id="desc"
                                        maxLength={280}
                                        value={this.state.service.resume}
                                        onChange={this.handleInputChange}
                                    />
                                    <span className="char-count">{280 - this.state.service.resume.length}</span> caracteres restantes
                                </Col>
                                <Col md="4" xs="12">
                                    <InputMask
                                        mask="(99) 99999 - 9999"
                                        type="tel"
                                        required={true}
                                        placeholder="Telefone para contato"
                                        name="service_phone"
                                        label="Telefone"
                                        id="tel"
                                        value={this.state.service.service_phone}
                                        onChange={this.handleInputChange}
                                    />
                                    <input
                                        required={true}
                                        placeholder="Email para contato"
                                        maxLength={50}
                                        type="email"
                                        name="service_email"
                                        label="Email"
                                        id="email"
                                        value={this.state.service.service_email}
                                        onChange={this.handleInputChange}
                                    />
                                    <CreateUpdateElements.btnContainer>
                                        <CreateUpdateElements.salvar>
                                            <button type="submit" className="btn btn-primary">
                                                Salvar
                                            </button>
                                        </CreateUpdateElements.salvar>
                                        <CreateUpdateElements.cancelar>
                                            <button type="button" className="btn btn-primary" onClick={this.toggle}>
                                                Cancelar
                                            </button>
                                        </CreateUpdateElements.cancelar>
                                    </CreateUpdateElements.btnContainer>
                                </Col>
                            </form>
                        </ModalBody>
                    </ModalElements.contentStyle>
                </Modal>

            </StructureLayout.Grid>
        );
    }
}
const mapStateToProps = state => ({

});

const mapDispatchToProps = dispatch => bindActionCreators({ deleteService, updateService }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(ServiceItem);