import styled from "styled-components";
import { Grid, Button } from '@material-ui/core'
import DefaultButton from "../../../../commons/components/default-button";
import { device } from "../../../../commons/devices"
//import { StructureLayout, Fonts } from "../../../../commons/layout";



export const ServiceElements = {
    container: styled.div`
        margin: 40px 0px 25px 0px;
        max-width: 1500px;
        text-align: left;
        display: flex;
        flex-direction: column;
        justify-content: center;
        word-wrap: break-word;
    `,
    service: styled.div`
        display: flex;
        flex-direction: row;
        max-width: 1060px;        
        justify-content: space-between;
    `,
    serviceText: styled.div`
        margin-right: 20px;
    `,
    options: styled.div`
        display: flex;
        flex-direction: column;
        justify-content: space-evenly;
        max-width: 100px;
        width: -webkit-fill-available;
        @media ${device.mobile}, ${device.miniTablet}, ${device.tablet} {
            justify-content: unset;
        }
    `,
    subTitle: styled.span`
        font-weight: 500;
        text-align: left;
    `,
    title: styled.h3`
        text-align: left;
    `,
    edit: styled(DefaultButton)`
        background-color: #0ac1ff !important;
        color: #FFF !important;
        height: 30px;
        width: 135px;
        margin-top: 10px;
        @media ${device.mobile}, ${device.miniTablet}, ${device.tablet} {
            width: 96px;    
            margin-bottom: 25px !important;
        }
    `,
    delete: styled(DefaultButton)`
        background-color: #E91E63 !important;
        color: #FFF !important;
        height: 30px;
        width: 135px;
        margin-top: 10px;
        @media ${device.mobile}, ${device.miniTablet}, ${device.tablet} {
            width: 96px;
        }
    `,
    elementContainer: styled(Grid)`
        margin-bottom: 40px !important;
    `
}