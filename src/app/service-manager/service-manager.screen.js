import React, { Component } from "react";
import { StructureLayout } from "../../commons/layout";
import MenuNavbar from "../../commons/containers/navbar";
import { Wrappers, CreateUpdateElements } from "./service-manager.style";
import Services from "./containers/services";
import { ModalElements } from '../../commons/components/modal/style';
import { createService } from "./service-manager.actions";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Modal, ModalHeader, ModalBody, Col } from 'reactstrap';
import InputMask from 'react-input-mask';

class ServiceManagerPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            service: {
                resume: ""
            }
        };
        this.toggle = this.toggle.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    onSubmit(event) {
        event.preventDefault();

        if (event.target.service_phone.value.replace(/[^0-9.]/g, "").length < 11) {
            alert("Por favor preencher número de telefone completo")
            return
        }
        if (event.target.resume.value.length < 180) {
            alert("Por favor no mínimo 180 caracteres")
            return
        }
        let service = {
            title: event.target.title.value,
            categories: event.target.categories.value,
            resume: event.target.resume.value,
            service_phone: event.target.service_phone.value,
            service_email: event.target.service_email.value

        }
        //TODO VALIDAR SE OS CAMPOS NAO ESTAO EM BRANCO
        this.props.createService(service);
        this.toggle();
        return null
    }

    toggle() {
        if (this.props.app.user.publishedServices.length < 3) {
            this.setState({
                modal: !this.state.modal,
                service: {
                    resume: ""
                }
            });
        } else {
            alert('Máximo de 3 serviços criados. Se deseja divulgar outro, altere ou apague um serviço.')
        }
    }

    handleInputChange(e) {
        this.setState({
            service: {
                ...this.state.service, [e.target.name]: e.target.value.toString()
            }
        })
    }

    render() {
        return (
            <div className="app-service-manager">
                <MenuNavbar />
                <Wrappers.header>
                    <Wrappers.myServices>
                        <Wrappers.title>Meus Serviços</Wrappers.title>
                        <button className="btn-primary btn-create" onClick={this.toggle}>Criar Serviço</button>
                    </Wrappers.myServices>
                </Wrappers.header>
                <StructureLayout.Container>
                    <Services />
                </StructureLayout.Container>
                {/* TODO MIGRAR ESSE MODAL PARA UM CONTAINER JUNTO COM O BOTAO "CRIAR SERVIÇO" */}
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalElements.contentStyle>
                        <ModalHeader toggle={this.toggle}>Anúnciar Serviço</ModalHeader>
                        <ModalBody>
                            <form className="formServiceModal" onSubmit={this.onSubmit}>
                                <Col md="8" xs="12">
                                    <CreateUpdateElements.flexDiv>
                                        <CreateUpdateElements.titleContainer>
                                            <input
                                                required={true}
                                                placeholder="Título"
                                                maxLength={20}
                                                name="title"
                                                label="Titulo"
                                                id="title"
                                            />
                                        </CreateUpdateElements.titleContainer>
                                        <CreateUpdateElements.inputSizer>
                                            <input
                                                required={true}
                                                placeholder="Sub-título"
                                                maxLength={20}
                                                name="categories"
                                                label="Categoria"
                                                id="category"
                                            />
                                        </CreateUpdateElements.inputSizer>
                                    </CreateUpdateElements.flexDiv>
                                    <textarea
                                        required={true}
                                        placeholder="Descreva um breve resumo sobre o serviço :)."
                                        name="resume"
                                        label="Descricao"
                                        rows="12"
                                        id="desc"
                                        value={this.state.service.resume}
                                        onChange={this.handleInputChange}
                                        maxLength={280}
                                    />
                                    <span className="char-count">{280 - this.state.service.resume.length}</span> caracteres restantes
                                </Col>
                                <Col md="4" xs="12">
                                    <InputMask
                                        mask="(99) 99999 - 9999"
                                        type="tel"
                                        required={true}
                                        placeholder="Telefone para contato"
                                        name="service_phone"
                                        label="Telefone"
                                        id="tel"
                                    />
                                    <input
                                        required={true}
                                        placeholder="Email para contato"
                                        maxLength={30}
                                        type="email"
                                        name="service_email"
                                        label="Email"
                                        id="email"
                                    />
                                    <CreateUpdateElements.btnContainer>
                                        <CreateUpdateElements.salvar>
                                            <button type="submit" className="btn btn-primary">
                                                Salvar
                                        </button>
                                        </CreateUpdateElements.salvar>
                                        <CreateUpdateElements.cancelar>
                                            <button type="button" className="btn btn-primary" onClick={this.toggle}>
                                                Cancelar
                                        </button>
                                        </CreateUpdateElements.cancelar>
                                    </CreateUpdateElements.btnContainer>
                                </Col>
                            </form>
                        </ModalBody>
                    </ModalElements.contentStyle>
                </Modal>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    app: state.app,
    session: state.session
});

const mapDispatchToProps = dispatch => bindActionCreators({ createService }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(ServiceManagerPage);