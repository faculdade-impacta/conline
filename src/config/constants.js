const envoriment = 'development';

const setEnv = (envoriment) => {
    if (envoriment === 'development') {
        return "http://127.0.0.1:5000/api/v1";
    }
    if (envoriment === 'production') {
        return "https://api.cuidador.online/api/v1";
    }
}

export const Constants = {
    application: {
        name: "Ope TCC"
    },
    api: setEnv(envoriment),
    amplify: {
        Auth: {
            identityPoolId: 'us-west-2:c3d2c5ff-e491-42a3-a745-32678f0493a9',
            region: 'us-west-2',
            identityPoolRegion: 'us-west-2',
            userPoolId: 'us-west-2_RDDQAuFFE',
            userPoolWebClientId: '6hr8b87m1fsrnkvvevouac69sf'
        }
    },
    errors: {
        500: "Internal server error"
    },
    imgUrl: "https://caas-images.s3.us-west-2.amazonaws.com/",
    UF: [
        { nome: "Acre", sigla: "AC" },
        { nome: "Alagoas", sigla: "AL" },
        { nome: "Amapá", sigla: "AP" },
        { nome: "Amazonas", sigla: "AM" },
        { nome: "Bahia", sigla: "BA" },
        { nome: "Ceará", sigla: "CE" },
        { nome: "Distrito Federal", sigla: "DF" },
        { nome: "Espírito Santo", sigla: "ES" },
        { nome: "Goiás", sigla: "GO" },
        { nome: "Maranhão", sigla: "MA" },
        { nome: "Mato Grosso", sigla: "MT" },
        { nome: "Mato Grosso do Sul", sigla: "MS" },
        { nome: "Minas Gerais", sigla: "MG" },
        { nome: "Pará", sigla: "PA" },
        { nome: "Paraíba", sigla: "PB" },
        { nome: "Paraná", sigla: "PR" },
        { nome: "Pernambuco", sigla: "PE" },
        { nome: "Piauí", sigla: "PI" },
        { nome: "Rio de Janeiro", sigla: "RJ" },
        { nome: "Rio Grande do Norte", sigla: "RN" },
        { nome: "Rio Grande do Sul", sigla: "RS" },
        { nome: "Rondônia", sigla: "RO" },
        { nome: "Roraima", sigla: "RR" },
        { nome: "Santa Catarina", sigla: "SC" },
        { nome: "São Paulo", sigla: "SP" },
        { nome: "Sergipe", sigla: "SE" },
        { nome: "Tocantins", sigla: "TO" }
    ]
}

export const ERROR = {
    401: "Sess�o invalida"
}
